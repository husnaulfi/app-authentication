package ulfiah.muhlishoh.appauthetication

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() , View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
        txSignUp.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnLogin -> {
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username Atau Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show()
                } else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "Berhasil Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, SignInActivity::class.java)
                            startActivity(intent)
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this, "Username Atau Password Salah", Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.txSignUp -> {
                var intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            }
        }
    }


}